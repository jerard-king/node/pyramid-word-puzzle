## Goal
The purpose of this effort was to create a script that decodes messages keyed by index and meant to viewed in a pyramid fashion where each row of indexes has 2 more rows items than and previous and only the last index in a pyramid is to be used

### Example input file:
```
   1 dog
   2 boy
   3 loves
   4 animal
   5 and
   6 boy
```
### Example Output
```
      1
    2   3
  4   5   6

dog loves boy
```
## Componenets
- Python
- IO
