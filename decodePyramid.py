# Author: Jerard King
# Date: 1/24/24
# Description: A script that decodes messages keyed by index and
#              meant to viewed in a pyramid fashion where each
#              row of indexes has 2 more rows items than and previous
#              and only the last index in a pyramid is to be used

# Example input file:
#   1 dog
#   2 boy
#   3 loves
#   4 animal
#   5 and
#   6 boy
# -- viewed as --
#      1
#    2   3
#  4   5   6

# Example output: dog loves boy

# build the work key/value dictionary and determine max key size
def getWordKeyDict(file_path):
  word_key_dictionary = {}
  max_word_key = 0
  with open(file_path, 'r', encoding='utf-8')  as myFile:
    for line in myFile:
      word_key,word_value = line.split()
      word_key_dictionary[word_key] = word_value

      if int(word_key, base=10) > int(max_word_key):
        max_word_key = word_key
    
  return (word_key_dictionary, int(max_word_key, base = 10))

# capture the target words from the last word in each pyramid level
def decodeWordKeyDict(word_key_dictionary, dictionary_size):
  decoded_word_list = []
  target_key_index = 0
  target_key_iterator = 0
  while target_key_index < dictionary_size:
    target_key_iterator += 1
    target_key_index = target_key_index + target_key_iterator
    print(target_key_index)
    decoded_word_list.append(word_key_dictionary[str(target_key_index)])

  return decoded_word_list
    
# lower case, clean up white spaces, and remove punctuation from the decoded message
def cleanupDecodedMessage(decoded_word_list):
  cleaned_message = ''
  for word in decoded_word_list:
    cleaned_message += word.lower() + ' '
  
  return cleaned_message.strip()

file_path = 'F:/Projects/dataAnnotation/decodePyramid/coding_qual_input.txt'
sample_file_dictionary, sample_file_max_index = getWordKeyDict(file_path)

decoded_message = decodeWordKeyDict(sample_file_dictionary, sample_file_max_index)
print (cleanupDecodedMessage(decoded_message))